
// GLOBAL VARIABLES
// Variables containing each character's values & user choices (currentPlayer and currentOpponent)
var cady = {
    name: "Cady",
    health: 120,
    power: 25,
    counterPower: 25
};
var regina = {
    name: "Regina",
    health: 140,
    power: 40,
    counterPower: 40
};
var karen = {
    name: "Karen",
    health: 120,
    power: 20,
    counterPower: 20
};
var gretchen = {
    name: "Gretchen",
    health: 130,
    power: 20,
    counterPower: 20
};
var currentPlayer;
var currentOpponent;
var playerChosen = false;

// DISPLAYS EACH CHARACTER
// adding cady
var cadyImage = $("<img>");
cadyImage.addClass("introImage");
cadyImage.attr("id", "cady");
cadyImage.attr("src", "./assets/images/cady.jpg");
$("#row-1").append(cadyImage);
// adding regina
var reginaImage = $("<img>");
reginaImage.addClass("introImage");
reginaImage.attr("id", "regina");
reginaImage.attr("src", "./assets/images/regina.jpg");
$("#row-1").append(reginaImage);
// adding karen
var karenImage = $("<img>");
karenImage.addClass("introImage");
karenImage.attr("id", "karen")
karenImage.attr("src", "./assets/images/karen.jpg");
$("#row-1").append(karenImage);
// adding gretchen
var gretchenImage = $("<img>");
gretchenImage.addClass("introImage");
gretchenImage.attr("id", "gretchen")
gretchenImage.attr("src", "./assets/images/gretchen.jpg");
$("#row-1").append(gretchenImage);

var playerSelect = $("<div>");
playerSelect.text("Choose Your Player");
playerSelect.addClass("playerText");
$("#row-1").append(playerSelect);

$(document).ready(function(){
// PLAYER OBJECTS
// Functions within this object execute when user clicks on #cady
var playerIsCady = {
    isCady: true,
    selectPlayer: function() {
        var target = $(event.target);
        if (target.is("#cady")) {
        currentPlayer = cady;
        playerChosen = true;
        $("#cady").addClass("playerSelect");
        playerSelect.text("Your player is " + cady.name + " - Health: " + cady.health);
        cadyImage.insertAfter(playerSelect);
        var opponents = $("#row-2");
        reginaImage.insertAfter(opponents);
        reginaImage.addClass("chooseOpponent");
        karenImage.insertAfter(opponents);
        karenImage.addClass("chooseOpponent");
        gretchenImage.insertAfter(opponents);
        gretchenImage.addClass("chooseOpponent");
        $("#row-2").text("Choose your opponent:");
        }
    },
    selectOpponent: function() {
        var target = $(event.target);
        if (playerChosen) {  
            // allows player to select opponent for each round
            $("#attack-button").click(attack);  
            if (target.is("#regina")) {
                currentOpponent = regina;
                reginaImage.addClass("defender");
                $("#row-3").text("Your defender is " + regina.name + " - Health: " + regina.health);
                reginaImage.insertAfter("#row-3");
            } else if (target.is("#cady")) {
                return;
            } else if (target.is("#karen")) {
                currentOpponent = karen;
                karenImage.addClass("defender");
                $("#row-3").text("Your defender is " + karen.name + " - Health: " + karen.health);
                karenImage.insertAfter("#row-3");
            } else if (target.is("#gretchen")) {
                currentOpponent = gretchen;
                gretchenImage.addClass("defender");
                $("#row-3").text("Your defender is " + gretchen.name + " - Health: " + gretchen.health);
                gretchenImage.insertAfter("#row-3");
            } 
        }
    }
}
// Functions within this object execute when user clicks on #regina
var playerIsRegina = {
    selectPlayer: function() {
        var target = $(event.target);
        if (target.is("#regina")) {
            currentPlayer = regina;
            playerChosen = true;
            $("#regina").addClass("playerSelect");
            playerSelect.text("Your player is " + regina.name + " - Health: " + regina.health);
            reginaImage.insertAfter(playerSelect);
            cadyImage.insertAfter("#row-2");
            cadyImage.addClass("chooseOpponent");
            karenImage.insertAfter("#row-2");
            karenImage.addClass("chooseOpponent");
            gretchenImage.insertAfter("#row-2");
            gretchenImage.addClass("chooseOpponent");
            $("#row-2").text("Choose your opponent:");
        }
    },
    selectOpponent: function() {
        var target = $(event.target);
        if (playerChosen) {
            // allows player to select opponent for each round
            $("#attack-button").click(attack);  
            if (target.is("#regina")) {
                return;
            } else if (target.is("#cady")) {
                currentOpponent = cady;
                cadyImage.addClass("defender");
                $("#row-3").text("Your defender is " + cady.name + " - Health: " + cady.health);
                cadyImage.insertAfter("#row-3");
            } else if (target.is("#karen")) {
                currentOpponent = karen;
                karenImage.addClass("defender");
                $("#row-3").text("Your defender is " + karen.name + " - Health: " + karen.health);
                karenImage.insertAfter("#row-3");
            } else if (target.is("#gretchen")) {
                currentOpponent = gretchen;
                gretchenImage.addClass("defender");
                $("#row-3").text("Your defender is " + gretchen.name + " - Health: " + gretchen.health);
                gretchenImage.insertAfter("#row-3");
            }
        }
    }
}
// Functions within this object execute when user clicks on #karen
var playerIsKaren = {
    selectPlayer: function() {
        var target = $(event.target);
        if (target.is("#karen")) {
            currentPlayer = karen;
            playerChosen = true;
            $("#karen").addClass("playerSelect");
            playerSelect.text("Your player is " + karen.name + " - Health: " + karen.health);
            karenImage.insertAfter(playerSelect);     
            reginaImage.insertAfter("#row-2");
            reginaImage.addClass("chooseOpponent");
            cadyImage.insertAfter("#row-2");
            cadyImage.addClass("chooseOpponent");
            gretchenImage.insertAfter("#row-2");
            gretchenImage.addClass("chooseOpponent");
            $("#row-2").text("Choose your opponent:");
        }
    },
    selectOpponent: function() {
        var target = $(event.target);
        if (playerChosen) {
            // allows player to select opponent for each round
            $("#attack-button").click(attack);  
            if (target.is("#karen")) {
                return;
            } else if (target.is("#regina")) {
                currentOpponent = regina;
                reginaImage.addClass("defender");
                $("#row-3").text("Your defender is " + regina.name + " - Health: " + regina.health);
                reginaImage.insertAfter("#row-3");
            } else if (target.is("#cady")) {
                currentOpponent = cady;
                cadyImage.addClass("defender");
                $("#row-3").text("Your defender is " + cady.name + " - Health: " + cady.health);
                cadyImage.insertAfter("#row-3");
            } else if (target.is("#gretchen")) {
                currentOpponent = gretchen;
                gretchenImage.addClass("defender");
                $("#row-3").text("Your defender is " + gretchen.name + " - Health: " + gretchen.health);
                gretchenImage.insertAfter("#row-3");
            }
        }
    }
}
// Functions within this object execute when user clicks on #gretchen
var playerIsGretchen = {
    selectPlayer: function() {
        var target = $(event.target);
        if (target.is("#gretchen")) {
            currentPlayer = gretchen;
            playerChosen = true;
            $("#gretchen").addClass("playerSelect");
            playerSelect.text("Your player is " + gretchen.name + " - Health: " + gretchen.health);
            gretchenImage.insertAfter(playerSelect);     
            reginaImage.insertAfter("#row-2");
            reginaImage.addClass("chooseOpponent");
            cadyImage.insertAfter("#row-2");
            cadyImage.addClass("chooseOpponent");
            karenImage.insertAfter("#row-2");
            karenImage.addClass("chooseOpponent");
            $("#row-2").text("Choose your opponent:");
        }
    },
    selectOpponent: function() {
        var target = $(event.target);
        if (playerChosen) {
            // allows player to select opponent for each round
            $("#attack-button").click(attack);  
            if (target.is("#gretchen")) {
                return;
            } else if (target.is("#regina")) {
                currentOpponent = regina;
                reginaImage.addClass("defender");
                $("#row-3").text("Your defender is " + regina.name + " - Health: " + regina.health);
                reginaImage.insertAfter("#row-3");
            } else if (target.is("#cady")) {
                currentOpponent = cady;
                cadyImage.addClass("defender");
                $("#row-3").text("Your defender is " + cady.name + " - Health: " + cady.health);
                cadyImage.insertAfter("#row-3");
            } else if (target.is("#karen")) {
                currentOpponent = karen;
                karenImage.addClass("defender");
                $("#row-3").text("Your defender is " + karen.name + " - Health: " + karen.health);
                karenImage.insertAfter("#row-3");
            }
        }
    }
}

// ATTACK FUNCTION
// reduces defender health by attack power and player health by defender's counter attack power
// user continues to click attack until one of the character's health is 0 or less
function attack() {
    currentPlayer.health -= currentOpponent.counterPower;
    currentOpponent.health -= currentPlayer.power;
    $("#row-2").text("");
    $("#row-4").html("Your health: " + currentPlayer.health + "<br>" + currentOpponent.name + "'s health: " + currentOpponent.health);
    if (currentPlayer.health <= 0) {
        $("#row-2").text("You lost! Click reset to play again.");
        // adding reset button, which runs reset function
        var reset = $("<button>");
        reset.attr("id", "reset-game");
        reset.text("Reset Game");
        reset.insertAfter("#attack-button");
        reset.click(resetGame);
        // "turns off" event for items so that the user must click reset
        $(".introImage").off("click");
        $("#attack-button").off("click");
    } else if (currentOpponent.health <= 0) {
        $("#row-2").text("You won! Select a new opponent: ");
        $("#attack-button").off("click");
        nextLevel();
        // Conditional executes if user has played all 3 rounds of the game
        if (currentPlayer === regina && 
            $("#cady").hasClass("defender") && 
            $("#karen").hasClass("defender") && 
            $("#gretchen").hasClass("defender")) {
            $("#row-3").text("You won the game! Click reset to play again.");
            $("#row-2, #row-4").text("");
            var reset = $("<button>");
            reset.attr("id", "reset-game");
            reset.text("Reset Game");
            reset.insertAfter("#attack-button");
            reset.click(resetGame);
        }
        if (currentPlayer === cady && 
            $("#regina").hasClass("defender") && 
            $("#karen").hasClass("defender") && 
            $("#gretchen").hasClass("defender")) {
            $("#row-3").text("You won the game! Click reset to play again.");
            $("#row-2, #row-4").text("");
            var reset = $("<button>");
            reset.attr("id", "reset-game");
            reset.text("Reset Game");
            reset.insertAfter("#attack-button");
            reset.click(resetGame);
        }
        if (currentPlayer === karen && 
            $("#cady").hasClass("defender") && 
            $("#regina").hasClass("defender") && 
            $("#gretchen").hasClass("defender")) {
            $("#row-3").text("You won the game! Click reset to play again.");
            $("#row-2, #row-4").text("");
            var reset = $("<button>");
            reset.attr("id", "reset-game");
            reset.text("Reset Game");
            reset.insertAfter("#attack-button");
            reset.click(resetGame);
        }
        if (currentPlayer === gretchen && 
            $("#cady").hasClass("defender") && 
            $("#karen").hasClass("defender") && 
            $("#regina").hasClass("defender")) {
            $("#row-3").text("You won the game! Click reset to play again.");
            $("#row-2, #row-4").text("");
            var reset = $("<button>");
            reset.attr("id", "reset-game");
            reset.text("Reset Game");
            reset.insertAfter("#attack-button");
            reset.click(resetGame);
        }
        // function to return to "choose your opponent page" and select new opponent
        // this function also changes the value of currentPlayer's attack power
    }
}

// NEXT LEVEL 
// Increments player's power by 25 and resets currentPlayer health
function nextLevel() {
    currentPlayer.power += 25;
    if (currentPlayer === cady) {
        currentPlayer.health = 120;
    } else if (currentPlayer === regina) {
        currentPlayer.health = 140;
    } else if (currentPlayer === karen) {
        currentPlayer.health = 120;
    } else if (currentPlayer === gretchen) {
        currentPlayer.health = 130;
    }
}

// Resets DOM and event listeners when user clicks "Reset Game" button
function resetGame() {
    // I know it's very lengthy and repetitive but I was unsure how to reset game without placing global variables inside a function, and making them unavailable to other functions
    $(".chooseOpponent, .playerText, .playerSelect, #reset-game").remove();
    $("#row-2, #row-3, #row-4").text("");
    cady = {
        name: "Cady",
        health: 120,
        power: 25,
        counterPower: 25
    };
    regina = {
        name: "Regina",
        health: 140,
        power: 40,
        counterPower: 40
    };
    karen = {
        name: "Karen",
        health: 120,
        power: 20,
        counterPower: 20
    };
    gretchen = {
        name: "Gretchen",
        health: 130,
        power: 20,
        counterPower: 20
    };
    currentPlayer;
    currentOpponent;
    playerChosen = false;
    
    // DISPLAYS EACH AVAIABLE CHARACTER
    // adding cady
    cadyImage = $("<img>");
    cadyImage.addClass("introImage");
    cadyImage.attr("id", "cady");
    cadyImage.attr("src", "./assets/images/cady.jpg");
    $("#row-1").append(cadyImage);
    // adding regina
    reginaImage = $("<img>");
    reginaImage.addClass("introImage");
    reginaImage.attr("id", "regina");
    reginaImage.attr("src", "./assets/images/regina.jpg");
    $("#row-1").append(reginaImage);
    // adding karen
    karenImage = $("<img>");
    karenImage.addClass("introImage");
    karenImage.attr("id", "karen")
    karenImage.attr("src", "./assets/images/karen.jpg");
    $("#row-1").append(karenImage);
    // adding gretchen
    gretchenImage = $("<img>");
    gretchenImage.addClass("introImage");
    gretchenImage.attr("id", "gretchen")
    gretchenImage.attr("src", "./assets/images/gretchen.jpg");
    $("#row-1").append(gretchenImage);
    
    playerSelect = $("<div>");
    playerSelect.text("Choose Your Player");
    playerSelect.addClass("playerText");
    $("#row-1").append(playerSelect);

    $("#cady").one("click", function(e) {
        var target = $(e.target);
        if (target.is("#cady")) {
            playerIsCady.selectPlayer();
            $("#cady, #regina, #karen, #gretchen").off("click");
            $("#regina, #karen, #gretchen").click(playerIsCady.selectOpponent);
        }
    });
    
    $("#regina").one("click", function(e) {
        var target = $(e.target);
        if (target.is("#regina")) {
            playerIsRegina.selectPlayer();
            $("#cady, #regina, #karen, #gretchen").off("click");
            $("#cady, #karen, #gretchen").click(playerIsRegina.selectOpponent);
        }
    });
    
    $("#karen").one("click", function(e) {
        var target = $(e.target);
        if (target.is("#karen")) {
            playerIsKaren.selectPlayer();
            $("#cady, #regina, #karen, #gretchen").off("click");
            $("#cady, #regina, #gretchen").click(playerIsKaren.selectOpponent);
        }
    });
    
    $("#gretchen").one("click", function(e) {
        var target = $(e.target);
        if (target.is("#gretchen")) {
            playerIsGretchen.selectPlayer();
            $("#cady, #regina, #karen, #gretchen").off("click");
            $("#cady, #regina, #karen").click(playerIsGretchen.selectOpponent);
        }
    });
    $("#attack-button").click(attack);  
}

// Event listeners that execute depending on the player clicked
$("#cady").one("click", function(e) {
    var target = $(e.target);
    if (target.is("#cady")) {
        playerIsCady.selectPlayer();
        $("#cady, #regina, #karen, #gretchen").off("click");
        $("#regina, #karen, #gretchen").click(playerIsCady.selectOpponent);
    }
});

$("#regina").one("click", function(e) {
    var target = $(e.target);
    if (target.is("#regina")) {
        playerIsRegina.selectPlayer();
        $("#cady, #regina, #karen, #gretchen").off("click");
        $("#cady, #karen, #gretchen").click(playerIsRegina.selectOpponent);
    }
});

$("#karen").one("click", function(e) {
    var target = $(e.target);
    if (target.is("#karen")) {
        playerIsKaren.selectPlayer();
        $("#cady, #regina, #karen, #gretchen").off("click");
        $("#cady, #regina, #gretchen").click(playerIsKaren.selectOpponent);
    }
});

$("#gretchen").one("click", function(e) {
    var target = $(e.target);
    if (target.is("#gretchen")) {
        playerIsGretchen.selectPlayer();
        $("#cady, #regina, #karen, #gretchen").off("click");
        $("#cady, #regina, #karen").click(playerIsGretchen.selectOpponent);
    }
});
});