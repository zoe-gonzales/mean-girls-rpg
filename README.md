# rpg-unit-4
## Mean Girls-themed RPG
### This is a role playing game with a theme based on the movie Mean Girls, utilizing Javascript and jQuery. 

### The font theme is from Google Fonts and the background design is from here: https://wallpapercave.com/mean-girls-wallpapers

### **Game Instructions**
1. Select one of the four players. Player health is not shown until that player's image is clicked.
2. Select an opponent from the three remaining players. The current defender will have a black border.
3. Click *attack* button to attack defender. Both player's health will decrease depending on that player's power and counter power.
4. If you lose, click *reset* button to play again. 
5. If you win, fight each remaining character to win the game. There's also the option to reset and play again once all three defenders have been beaten.

### **Game Screenshots**

#### Start Page
![Start Page](assets/images/start-page.png)

#### User chooses a player
![Player Chosen](assets/images/player-chosen.png)

#### User chooses an opponent from remaining players
![Opponent Chosen](assets/images/opponent-chosen.png)

#### First attack
![First Attack](assets/images/first-attack.png)

#### Several attacks later....
![Player Wins](assets/images/player-wins.png)

#### Or maybe they lose.
![Player Loses](assets/images/player-loses.png)

#### The reset game button at the bottom brings the user back to the start page.

#### Deployed at https://zoe-gonzales.github.io/rpg-unit-4/
